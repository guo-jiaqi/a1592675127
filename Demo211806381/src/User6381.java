
public class User6381 {
	
private String name;
private String password;
private String phone;
public User6381() {
	super();
}
public User6381(String name, String password, String phone) {
	super();
	this.name = name;
	this.password = password;
	this.phone = phone;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public String getPhone() {
	return phone;
}
public void setPhone(String phone) {
	this.phone = phone;
}
@Override
public String toString() {
	return "User6381 [name=" + name + ", password=" + password + ", phone=" + phone + "]";
}

}
